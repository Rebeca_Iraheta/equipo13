<?php 
class Competencia{
	private $nombreC;
	private $descr;

    /**
     * @return mixed
     */
    public function getNombreC()
    {
        return $this->nombreC;
    }

    /**
     * @param mixed $nombreC
     *
     * @return self
     */
    public function setNombreC($nombreC)
    {
        $this->nombreC = $nombreC;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * @param mixed $descr
     *
     * @return self
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }
}