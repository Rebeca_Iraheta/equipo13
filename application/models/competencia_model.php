<?php
defined('BASEPATH')OR exit ('No direct script acccess allowed');

class competencia_model extends CI_Model
{
	
	function __construct()
	{
		parent:: __construct();
		$this->load->library('competencia');
	}

	/*Insertar competencia*/
	public function create($competencia){
		
		$this->db->set('nombre',$competencia->getNombreC());
		$this->db->set('descripcion', $competencia->getDescr());
		$this->db->insert('competencia');
	}
}
?>